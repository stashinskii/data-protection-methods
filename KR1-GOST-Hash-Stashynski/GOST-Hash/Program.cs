﻿using System;
using System.IO;

namespace GOST_Hash
{
    class Program
    {
        static void Main(string[] args)
        {
            var message = File.ReadAllBytes("../herman_stashynski.txt");
            StribogHasher hasher = new StribogHasher();
            var hash = hasher.GetHash(message, true);
            var hashCode = hasher.GetHashStringFormat(message, true);

            File.WriteAllBytes("../herman_stashynski_HASH", hash);
            File.WriteAllText("../herman_stashynski_HASH-FORMATTED", hashCode);

            foreach (var byteValue in hash)
            {
                Console.Write(byteValue);
            }

            Console.WriteLine("\n");
            Console.WriteLine(hashCode);
        }
    }
}
