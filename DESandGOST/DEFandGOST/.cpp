#include <cstdint>
#include <iostream>
#include <string>

#include "headers/block.h"
#include "headers/triple.h"
#include <vector>


/*
int main() {
  std::vector<uint8_t> src = prompt_msg();
  std::vector<uint8_t> key = prompt_key(24);

  TripleDESCipher dc(key);

  std::vector<uint8_t> dst1 = dc.Encrypt(src);
  std::cout << "enc: " << std::hex;
  for (std::size_t i = 0; i < dst1.size(); i++) {
    std::cout << (uint64_t)(dst1[i]) << " ";
  }
  std::cout << std::dec << std::endl;

  std::vector<uint8_t> dst2 = dc.Decrypt(dst1);
  std::cout << "dec: " << std::hex;
  for (std::size_t i = 0; i < dst2.size(); i++) {
    std::cout << (uint64_t)(dst2[i]) << " ";
  }
  std::cout << std::dec << std::endl;

  std::cout << "dec(text): ";
  for (std::size_t i = 0; i < dst2.size(); i++) {
    std::cout << (char)(dst2[i]);
  }
  std::cout << std::endl;

  return 0;
}

*/