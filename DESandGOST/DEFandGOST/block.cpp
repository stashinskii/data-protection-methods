#include <cassert>
#include <cstdint>
#include <cstring>
#include <utility>
#include <vector>

#include "headers/block.h"
#include "headers/const.h"

uint32_t feistelBox[8][64] = {{0}};
bool feistelBoxInited = false;

void crypt_block(vector<uint64_t> &subkeys, std::vector<uint8_t> &dst,
                vector<uint8_t> src, bool decrypt) {
  uint64_t b = 0;
  assert(src.size() == 8);
  for (int i = 0; i < 8; i++) {
    b += (static_cast<uint64_t>(src[i]) << 8 * (7 - i));
  }

  b = permuteInitial_block(b);

  uint32_t left = static_cast<uint32_t>(b >> 32);
  uint32_t right = static_cast<uint32_t>(b);

  left = (left << 1) | (left >> 31);
  right = (right << 1) | (right >> 31);

  if (decrypt) {
    for (uint8_t i = 0; i < 8; i++) {
      auto pair =
          feistel(left, right, subkeys[15 - 2 * i], subkeys[15 - (2 * i + 1)]);
      left = pair.first;
      right = pair.second;
    }
  } else {
    for (uint8_t i = 0; i < 8; i++) {
      auto pair = feistel(left, right, subkeys[2 * i], subkeys[2 * i + 1]);
      left = pair.first;
      right = pair.second;
    }
  }

  left = (left << 31) | (left >> 1);
  right = (right << 31) | (right >> 1);

  // switch left & right and perform final permutation
  uint64_t preOutput =
      (static_cast<uint64_t>(right) << 32) | static_cast<uint64_t>(left);

  uint64_t out = permute_final_block(preOutput);
  assert(dst.size() == 8);
  for (int i = 0; i < 8; i++) {
    dst[i] = (out >> 8 * (7 - i));
  }
}

void encrypt_block(vector<uint64_t> &subkeys, vector<uint8_t> &dst,
                  const vector<uint8_t> &src) {
  crypt_block(subkeys, dst, src, false);
}

void decrypt_block(vector<uint64_t> &subkeys, vector<uint8_t> &dst,
                  const vector<uint8_t> &src) {
  crypt_block(subkeys, dst, src, true);
}

pair<uint32_t, uint32_t> feistel(uint32_t l, uint32_t r, uint64_t k0,
                                      uint64_t k1) {
  uint32_t t = 0;

  t = r ^ static_cast<uint32_t>(k0 >> 32);
  l ^= feistelBox[7][t & 0x3f] ^ feistelBox[5][(t >> 8) & 0x3f] ^
       feistelBox[3][(t >> 16) & 0x3f] ^ feistelBox[1][(t >> 24) & 0x3f];

  t = ((r << 28) | (r >> 4)) ^ static_cast<uint32_t>(k0);
  l ^= feistelBox[6][(t)&0x3f] ^ feistelBox[4][(t >> 8) & 0x3f] ^
       feistelBox[2][(t >> 16) & 0x3f] ^ feistelBox[0][(t >> 24) & 0x3f];

  t = l ^ static_cast<uint32_t>(k1 >> 32);
  r ^= feistelBox[7][t & 0x3f] ^ feistelBox[5][(t >> 8) & 0x3f] ^
       feistelBox[3][(t >> 16) & 0x3f] ^ feistelBox[1][(t >> 24) & 0x3f];

  t = ((l << 28) | (l >> 4)) ^ static_cast<uint32_t>(k1);
  r ^= feistelBox[6][(t)&0x3f] ^ feistelBox[4][(t >> 8) & 0x3f] ^
       feistelBox[2][(t >> 16) & 0x3f] ^ feistelBox[0][(t >> 24) & 0x3f];

  return std::make_pair(l, r);
}

uint64_t permute_block(uint64_t src, const vector<uint8_t> &permutation) {
  uint64_t block = 0;
  size_t len = permutation.size();
  for (uint8_t position = 0; position < len; position++) {
    uint8_t n = permutation[position];
    uint64_t bit = (src >> n) & 1;
    block |= (bit << static_cast<uint64_t>((len - 1) - position));
  }
  return block;
}

void init_feistel_box() {
  for (uint8_t s = 0; s < 8; s++) {
    for (uint8_t i = 0; i < 4; i++) {
      for (uint8_t j = 0; j < 16; j++) {
        uint64_t f = static_cast<uint64_t>(Sboxes[s][i][j])
                     << (4 * (7 - static_cast<unsigned long>(s)));
        f = permute_block(f, permutation_function);

        uint8_t row = static_cast<uint8_t>(((i & 2) << 4) | (i & 1));
        uint8_t col = static_cast<uint8_t>(j << 1);
        uint8_t t = row | col;

        f = (f << 1) | (f >> 31);

        feistelBox[s][t] = static_cast<uint32_t>(f);
      }
    }
  }
}

uint64_t permuteInitial_block(uint64_t block) {
  uint64_t b1 = block >> 48;
  uint64_t b2 = block << 48;
  block ^= b1 ^ b2 ^ b1 << 48 ^ b2 >> 48;

  b1 = (block >> 32 & 0xff00ff);
  b2 = (block & 0xff00ff00);
  block ^=
      (b1 << 32) ^ b2 ^ (b1 << 8) ^ (b2 << 24); 


  b1 = block & 0x0f0f00000f0f0000;
  b2 = block & 0x0000f0f00000f0f0;
  block ^= b1 ^ b2 ^ (b1 >> 12) ^ (b2 << 12);

  b1 = block & 0x3300330033003300;
  b2 = block & 0x00cc00cc00cc00cc;
  block ^= b1 ^ b2 ^ (b1 >> 6) ^ (b2 << 6);


  b1 = block & 0xaaaaaaaa55555555;
  block ^= b1 ^ (b1 >> 33) ^ (b1 << 33);

  return block;
}

uint64_t permute_final_block(uint64_t block) {
  uint64_t b1 = block & 0xaaaaaaaa55555555;
  block ^= b1 ^ (b1 >> 33) ^ (b1 << 33);

  b1 = block & 0x3300330033003300;
  uint64_t b2 = block & 0x00cc00cc00cc00cc;
  block ^= b1 ^ b2 ^ (b1 >> 6) ^ (b2 << 6);

  b1 = block & 0x0f0f00000f0f0000;
  b2 = block & 0x0000f0f00000f0f0;
  block ^= b1 ^ b2 ^ (b1 >> 12) ^ (b2 << 12);

  b1 = (block >> 32) & 0xff00ff;
  b2 = (block & 0xff00ff00);
  block ^= (b1 << 32) ^ b2 ^ (b1 << 8) ^ (b2 << 24);

  b1 = block >> 48;
  b2 = block << 48;
  block ^= b1 ^ b2 ^ (b1 << 48) ^ (b2 >> 48);
  return block;
}

std::vector<uint32_t> ks_rotate(uint32_t in) {
  auto out = std::vector<uint32_t>(16);
  uint32_t last = in;
  for (uint8_t i = 0; i < 16; i++) {
    uint32_t left = (last << (4 + rotations_KS[i])) >> 4;
    uint32_t right = (last << 4) >> (32 - rotations_KS[i]);
    out[i] = left | right;
    last = out[i];
  }
  return out;
}

uint64_t unpack(uint64_t x) {
  return ((x >> (6 * 1)) & 0xff) << (8 * 0) |
         ((x >> (6 * 3)) & 0xff) << (8 * 1) |
         ((x >> (6 * 5)) & 0xff) << (8 * 2) |
         ((x >> (6 * 7)) & 0xff) << (8 * 3) |
         ((x >> (6 * 0)) & 0xff) << (8 * 4) |
         ((x >> (6 * 2)) & 0xff) << (8 * 5) |
         ((x >> (6 * 4)) & 0xff) << (8 * 6) |
         ((x >> (6 * 6)) & 0xff) << (8 * 7);
}
