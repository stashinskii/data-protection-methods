#include "cipher.h"

class TripleDES {
  DESCipher *d1, *d2, *d3;

  const uint8_t blockSize = 8;

  vector<uint8_t> crypt(const vector<uint8_t> &msg, bool encrypt);

  void encrypt(vector<uint8_t> &dst, const vector<uint8_t> &src);
  void decrypt(vector<uint8_t> &dst, const vector<uint8_t> &src);

public:
  TripleDES(const vector<uint8_t> &key);

  vector<uint8_t> Encrypt_3DES(const vector<uint8_t> &msg);
  vector<uint8_t> Decrypt_3DES(const vector<uint8_t> &msg);

  ~TripleDES();
};
