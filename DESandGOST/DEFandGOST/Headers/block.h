#include <utility>
#include <vector>

using namespace std;

void crypt_block(const vector<uint64_t> &subkeys, vector<uint8_t> &dst,
                vector<uint8_t> src, bool decrypt);

void encrypt_block(vector<uint64_t> &subkeys, vector<uint8_t> &dst,
                  const vector<uint8_t> &src);

void decrypt_block(vector<uint64_t> &subkeys, vector<uint8_t> &dst,
                  const vector<uint8_t> &src);

pair<uint32_t, uint32_t> feistel(uint32_t l, uint32_t r, uint64_t k0,
                                      uint64_t k1);

extern uint32_t feistelBox[8][64];
extern bool feistelBoxInited;

uint64_t permute_block(uint64_t src, const vector<uint8_t> &permutation);

void init_feistel_box();

uint64_t permuteInitial_block(uint64_t block);

uint64_t permute_final_block(uint64_t block);

vector<uint32_t> ks_rotate(uint32_t in);

uint64_t unpack(uint64_t x);
