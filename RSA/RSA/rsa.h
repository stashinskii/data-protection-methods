#pragma once

#include <cmath>
#include <vector>

using namespace std;

uint64_t gcd(uint64_t a, uint64_t b);
bool is_prime(uint64_t n);
uint64_t calc_e(uint64_t t);
uint64_t calc_d(uint64_t e, uint64_t t);
vector<uint64_t> encrypt(const vector<uint64_t> &msg, uint64_t e, uint64_t n);
vector<uint64_t> decrypt(const vector<uint64_t> &enc, uint64_t d, uint64_t n);
